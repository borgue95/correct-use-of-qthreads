#include "MainWindow.h"
#include "code/BadThread.h"
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->badButton, &QPushButton::clicked, this, &MainWindow::onBadButtonClicked);
    connect(ui->goodButton, &QPushButton::clicked, this, &MainWindow::onGoodButtonClicked);
}

MainWindow::~MainWindow() {

}

void MainWindow::onBadButtonClicked() {
    std::cout << "Say hello! Hello!" << std::endl;

    BadThread *badThread = new BadThread(this);
    badThread->start();
}

void MainWindow::onGoodButtonClicked() {
    std::cout << "Good button clicked" << std::endl;

    // CameraValidationWidget

    thread = new QThread();
    testWorker = new TestWorker();
    testWorker->moveToThread(thread);

    // basic connects
    connect(thread, &QThread::started, testWorker, &TestWorker::init); // the order of connects matter
    connect(thread, &QThread::started, testWorker, &TestWorker::do_work);
    connect(testWorker, &TestWorker::work_done, thread, &QThread::quit);
    connect(testWorker, &TestWorker::work_done, testWorker, &TestWorker::deleteLater);
    connect(thread, &QThread::finished, thread, &QThread::deleteLater);

    thread->start();
}
