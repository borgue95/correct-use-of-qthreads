#include "CaptureWorker.h"
#include <iostream>
#include <QThread>
#include <QMetaObject>

CaptureWorker::CaptureWorker(QObject *parent) : QObject(parent) {
    m_stop = false;
}

CaptureWorker::~CaptureWorker() {
    std::cout << "CaptureWorker: deleted!" << std::endl;
}

void CaptureWorker::init() {
    std::cout << "CaptureWorker: Initializing my own QObjects..." << std::endl;
    emit inited();
}

void CaptureWorker::grab_frames() {
    std::cout << "CaptureWorker: capturing 1 frame..." << std::endl;
    QThread::sleep(1);

    if (!m_stop) {
        QMetaObject::invokeMethod(this, "grab_frames", Qt::QueuedConnection);
    } else {
        std::cout << "CaptureWorker: done! bye" << std::endl;
        emit work_done();
    }

}

void CaptureWorker::stop() {
    m_stop = true;
}
