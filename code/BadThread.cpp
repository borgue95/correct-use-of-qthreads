#include "BadThread.h"
#include "code/CaptureWorker.h"
#include <iostream>

BadThread::BadThread(QObject *parent) : QThread(parent) {

}

BadThread::~BadThread() {

}

void BadThread::run() {
    std::cout << "BadThread: lots of blocking stuff to do" << std::endl;
    QThread::sleep(1);
    std::cout << "BadThread: lots of blocking stuff to do" << std::endl;
    QThread::sleep(1);

    std::cout << "BadThread: lets create a proper thread:" << std::endl;
    CaptureWorker *worker = new CaptureWorker();
    QThread *thread = new QThread();
    worker->moveToThread(thread);
    connect(thread, &QThread::started, worker, &CaptureWorker::grab_frames);
    thread->start();

    QThread::sleep(3);
    worker->stop();

    std::cout << "BadThread: I'm done" << std::endl;
}
