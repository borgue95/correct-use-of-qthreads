#ifndef QTHREADMISTERY_BADTHREAD_H
#define QTHREADMISTERY_BADTHREAD_H

#include <QThread>

class BadThread : public QThread {
    Q_OBJECT

public:
    BadThread(QObject *parent = nullptr);
    ~BadThread();

protected:
    void run();

};


#endif //QTHREADMISTERY_BADTHREAD_H
