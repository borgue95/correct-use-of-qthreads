#ifndef QTHREADMISTERY_MAINWINDOW_H
#define QTHREADMISTERY_MAINWINDOW_H

#include <QMainWindow>
#include "code/TestWorker.h"

#include "ui_MainWindow.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onBadButtonClicked();
    void onGoodButtonClicked();

private:
    Ui::MainWindow *ui;

    QThread *thread;
    TestWorker *testWorker;

};



#endif //QTHREADMISTERY_MAINWINDOW_H
