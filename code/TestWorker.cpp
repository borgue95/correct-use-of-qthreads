#include "TestWorker.h"
#include <iostream>


TestWorker::TestWorker(QObject *parent) : QObject(parent) {
    thread = nullptr;
    captureWorker = nullptr;
}

TestWorker::~TestWorker() {
    std::cout << "TestWorker: deleted!" << std::endl;
}

void TestWorker::init() {
    std::cout << "TestWorker: initializing my own QObjects" << std::endl;
    thread = new QThread();
    captureWorker = new CaptureWorker();
}

void TestWorker::do_work() {
    std::cout << "TestWorker: lots of blocking tasks to do. Lets go" << std::endl;
    std::cout << "TestWorker: task 1" << std::endl;
    QThread::sleep(1);
    std::cout << "TestWorker: task 2" << std::endl;
    QThread::sleep(1);
    std::cout << "TestWorker: task 3" << std::endl;
    QThread::sleep(1);

    std::cout << "TestWorker: lets do the active waiting task:" << std::endl;
    captureWorker->moveToThread(thread);

    // basic connects
    connect(thread, &QThread::started, captureWorker, &CaptureWorker::init); // the order of connects matters
    connect(thread, &QThread::started, captureWorker, &CaptureWorker::grab_frames);
    connect(captureWorker, &CaptureWorker::work_done, thread, &QThread::quit);
    connect(captureWorker, &CaptureWorker::work_done, captureWorker, &CaptureWorker::deleteLater);
    connect(thread, &QThread::finished, thread, &QThread::deleteLater);

    // extra connects
    connect(captureWorker, &CaptureWorker::inited, this, &TestWorker::onWorkerInited);

    thread->start();
}

void TestWorker::onWorkerInited() {
    std::cout << "TestWorker: CaptureWorker inited. I will wait 4 seconds..." << std::endl;

    // wait 4 seconds of frame grabbing
    QThread::sleep(4);

    // here, get the last valid frame (a copy. the owner should be this, not the worker)

    std::cout << "TestWorker: stopping CaptureWorker..." << std::endl;
    QMetaObject::invokeMethod(captureWorker, "stop", Qt::QueuedConnection);

    // make here the calculations

    std::cout << "TestWorker: I'm done" << std::endl;
    emit work_done();
}
