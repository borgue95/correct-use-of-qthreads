#ifndef QTHREADMISTERY_TESTWORKER_H
#define QTHREADMISTERY_TESTWORKER_H

#include <QObject>
#include <QThread>
#include "code/CaptureWorker.h"

class TestWorker : public QObject {
    Q_OBJECT

public:
    TestWorker(QObject *parent = nullptr);
    ~TestWorker();

public slots:
    void init();
    void do_work();

private slots:
    void onWorkerInited();

signals:
    void work_done();

private:
    QThread *thread;
    CaptureWorker *captureWorker;

};


#endif //QTHREADMISTERY_TESTWORKER_H
