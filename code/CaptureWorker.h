#ifndef QTHREADMISTERY_CAPTUREWORKER_H
#define QTHREADMISTERY_CAPTUREWORKER_H

#include <QObject>

class CaptureWorker : public QObject {
    Q_OBJECT

public:
    CaptureWorker(QObject *parent = nullptr);
    ~CaptureWorker();

public slots:
    void init();
    void grab_frames();
    void stop();

signals:
    void inited();
    void work_done();

private:
    bool m_stop;

};


#endif //QTHREADMISTERY_CAPTUREWORKER_H
